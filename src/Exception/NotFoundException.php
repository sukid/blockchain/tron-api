<?php
namespace Suzzzj\TronAPI\Exception;

use InvalidArgumentException;

class NotFoundException extends InvalidArgumentException
{
    //
}