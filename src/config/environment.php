<?php
return [
    'shasta'=>[
        'fullnode'=>'https://api.shasta.trongrid.io',
    ],
    'mainnet'=>[
        'fullnode'=>'https://api.trongrid.io',
    ],
    'nile'=>[
        'fullnode'=>'https://api.nileex.io',
    ],
    'tronex'=>[
        'fullnode'=>'https://testhttpapi.tronex.io',
    ],
];