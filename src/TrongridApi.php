<?php


namespace Suzzzj\TronAPI;


class TrongridApi
{

    /**
     * Base Tron object
     *
     * @var Tron
     */
    protected $tron;

    /**
     * Create an TransactionBuilder object
     *
     * @param Tron $tron
     */
    public function __construct(Tron $tron)
    {
        $this->tron = $tron;
    }

    public function transactions($contract_address,$param=[],$fingerprint=""){
        $action="/v1/contracts/{$contract_address}/transactions";
        $query=[
            'min_block_timestamp'=>strtotime("2021-07-01 00:00:00") * 1000,
            'order_by' => 'block_timestamp,asc',
            'limit' => 50,
        ];
        if ($fingerprint) {
            $query['fingerprint'] = $fingerprint;
        }
        return [
            'action'=>$action,
            'query'=>$query
        ];
    }

}