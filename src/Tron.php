<?php

namespace Suzzzj\TronAPI;

use Suzzzj\TronAPI\Exception\TronException;
use Elliptic\EC;
use Suzzzj\TronAPI\Support\Keccak;
use Suzzzj\TronAPI\Support\Base58;
use Suzzzj\TronAPI\Support\Base58Check;
use Suzzzj\TronAPI\Support\Crypto;
use Suzzzj\TronAPI\Support\Hash;
use Suzzzj\TronAPI\Support\Utils;
use Suzzzj\TronAPI\Provider\HttpProviderInterface;


class Tron
{
    use TronAwareTrait;
    const ADDRESS_SIZE = 34;
    const ADDRESS_PREFIX = "41";
    const ADDRESS_PREFIX_BYTE = 0x41;

    public $address = [
        'base58' => null,
        'hex' => null
    ];

    /**
     * Provider manager
     *
     * @var TronManager
     */
    protected $manager;

    /**
     * Private key
     *
     * @var string
     */
    protected $privateKey;

    /**
     * Transaction Builder
     *
     * @var TransactionBuilder
     */
    protected $transactionBuilder;
    /**
     * TrongridApi Builder
     *
     * @var TrongridApi
     */
    protected $trongridApi;

    /**
     * Create a new Tron object
     *
     * @param HttpProviderInterface $fullNode
     * @param HttpProviderInterface $solidityNode
     * @param HttpProviderInterface|null $eventServer
     * @param HttpProviderInterface|null $signServer
     * @param HttpProviderInterface|null $explorer
     * @param string $privateKey
     * @throws TronException
     */
    public function __construct($environment = 'mainnet',
                                ?string $privateKey = null)
    {
        if (!is_null($privateKey)) {
            $this->setPrivateKey($privateKey);
        }
        $EnvironmentConfig = include_once __DIR__ . "./config/environment.php";
        $fullNode = new \Suzzzj\TronAPI\Provider\HttpProvider($EnvironmentConfig[$environment]["fullnode"]);

        $this->setManager(new TronManager($this, [
            'fullNode' => $fullNode,
            'solidityNode' => $fullNode,
            'eventServer' => $fullNode,
            'signServer' => $fullNode,
        ]));

        $this->transactionBuilder = new TransactionBuilder($this);
        $this->trongridApi = new TrongridApi($this);
    }

    /**
     * Enter the link to the manager nodes
     *
     * @param $providers
     */
    public function setManager($providers)
    {
        $this->manager = $providers;
    }

    /**
     * Enter your private account key
     *
     * @param string $privateKey
     */
    public function setPrivateKey(string $privateKey): void
    {
        $this->privateKey = $privateKey;
    }

    /**
     * Create a new account
     *
     * @return TronAddress
     * @throws TronException
     */
    public function createAccount(): TronAddress
    {
        return $this->generateAddress();
    }

    /**
     * Generate new address
     *
     * @return TronAddress
     * @throws TronException
     */
    public function generateAddress(): TronAddress
    {
        $ec = new EC('secp256k1');

        // Generate keys
        $key = $ec->genKeyPair();
        $priv = $ec->keyFromPrivate($key->priv);
        $pubKeyHex = $priv->getPublic(false, "hex");

        $pubKeyBin = hex2bin($pubKeyHex);
        $addressHex = $this->getAddressHex($pubKeyBin);
        $addressBin = hex2bin($addressHex);
        $addressBase58 = $this->getBase58CheckAddress($addressBin);

        return new TronAddress([
            'private_key' => $priv->getPrivate('hex'),
            'public_key' => $pubKeyHex,
            'address_hex' => $addressHex,
            'address_base58' => $addressBase58
        ]);
    }

    public function getAddressHex(string $pubKeyBin): string
    {
        if (strlen($pubKeyBin) == 65) {
            $pubKeyBin = substr($pubKeyBin, 1);
        }

        $hash = Keccak::hash($pubKeyBin, 256);

        return self::ADDRESS_PREFIX . substr($hash, 24);
    }

    public function getBase58CheckAddress(string $addressBin): string
    {
        $hash0 = Hash::SHA256($addressBin);
        $hash1 = Hash::SHA256($hash0);
        $checksum = substr($hash1, 0, 4);
        $checksum = $addressBin . $checksum;

        return Base58::encode(Crypto::bin2bc($checksum));
    }


    /**
     * Validate Tron Address (Locale)
     *
     * @param string|null $address
     * @return bool
     */
    public function isAddress(string $address = null): bool
    {
        if (strlen($address) !== self::ADDRESS_SIZE)
            return false;

        $address = Base58Check::decode($address, 0, 0, false);
        $utf8 = hex2bin($address);

        if (strlen($utf8) !== 25) return false;
        if (strpos($utf8, self::ADDRESS_PREFIX_BYTE) !== 0) return false;

        $checkSum = substr($utf8, 21);
        $address = substr($utf8, 0, 21);

        $hash0 = Hash::SHA256($address);
        $hash1 = Hash::SHA256($hash0);
        $checkSum1 = substr($hash1, 0, 4);

        if ($checkSum === $checkSum1)
            return true;
        return false;
    }

    /**
     * Enter your account address
     *
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $_toHex = $this->address2HexString($address);
        $_fromHex = $this->hexString2Address($address);

        $this->address = [
            'hex' => $_toHex,
            'base58' => $_fromHex
        ];
    }

    /**
     * Getting a balance
     *
     * @param string $address
     * @param bool $fromTron
     * @return float
     * @throws TronException
     */
    public function getBalance(string $address = null, bool $fromTron = false): float
    {
        $account = $this->getAccount($address);

        if (!array_key_exists('balance', $account)) {
            return 0;
        }

        return ($fromTron == true ?
            $this->fromTron($account['balance']) :
            $account['balance']);
    }

    /**
     * Query information about an account
     *
     * @param $address
     * @return array
     * @throws TronException
     */
    public function getAccount(string $address = null): array
    {
        $address = (!is_null($address) ? $this->toHex($address) : $this->address['hex']);

        return $this->manager->request('walletsolidity/getaccount', [
            'address' => $address
        ]);
    }

    public function trongrid_transactions($contract_address, $param = [], $fingerprint = "")
    {
        $query = [
//            'min_block_timestamp'=>strtotime("2021-07-01 00:00:00") * 1000,
            'order_by' => 'block_timestamp,desc',
            'limit' => 1,
        ];
        $response = $this->manager->request("v1/contracts/{$contract_address}/transactions", $query);
        return $response;
    }

    public function decodeInputParameters($abi, $func, $content)
    {
        $data = [];
        $inputTypes = [];
        foreach ($abi as $key => $item) {
            if (isset($item['name']) && $item['name'] === $func) {
                foreach ($item['inputs'] as $input) {
                    if (isset($input['type'])) {
                        $inputTypes[] = $input['type'];
                        $offset = empty($data) ? 0 : $data[count($data) - 1]['offset'] + $data[count($data) - 1]['length'];
                        $data[] = [
                            'name' => $input['name'],
                            'type' => $input['type'],
                            'length' => 64,
                            'value' => '',
                            'offset' => $offset,
                        ];
                    }
                }
                break;
            }
        }

        $prefix = substr(\Suzzzj\TronAPI\Support\Keccak::hash("{$func}(" . implode(",", $inputTypes) . ")", 256), 0, 8);
        $contect_prefix = substr($content, 0, 8);
        if ($contect_prefix != $prefix) {
            return false;
        }

        $content = substr($content, 8);

        foreach ($data as $key => $value) {
            $n = gmp_init(substr($content, $value['offset'], $value['length']), 16);
            switch ($value['type']){
                case 'uint256':
                    $n=gmp_strval($n,10);
                    break;
                case 'address':
                    $n=$this->hexString2Address("0x".gmp_strval($n,16));
                    break;
            }
            $data[$key]['value']=$n;
        }
        return $data;

    }


}